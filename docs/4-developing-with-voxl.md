---
layout: default
title: Developing with VOXL
nav_order: 4
has_children: true
permalink: /developing-with-voxl/
---

# Developing with VOXL
{: .no_toc }

This section contains instructions on how to use, install and configure components of the VOXL system along with other technical information aimed at helping users.

For reference, manifest of Qualcomm opensource code used in the VOXL system image: https://source.codeaurora.org/quic/le/le/manifest/tree/LE.UM.1.3.r4-06300-8x96.0.xml?h=release

{: .no_toc .text-delta }

1. TOC
{:toc}

---

# Software Stack

![software-stack.png](/images/resources/software-stack.png)

# System Architecture
![voxl-flight-m500-software-architecture-drone-ros-px4.png](/images/resources/voxl-flight-m500-software-architecture-drone-ros-px4.png)
