---
layout: default
title: Flight Core Getting Started
parent: Flight Core Manual
nav_order: 1
has_children: true
permalink: /flight-core-getting-started/
---

# Flight Core Getting Started

How to get started with ModalAI Flight Core
