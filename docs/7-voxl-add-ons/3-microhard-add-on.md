---
layout: default
title: Microhard Add-On Board Manual
parent: VOXL Add-Ons
nav_order: 3
permalink: /microhard-add-on-manual/
---

# Microhard Add-On Board Manual
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Overview

The VOXL system supports the ability to easily and quickly add a Microhard wireless connection between a vehicle and a Ground Control Station.  The following guide provides you the necessary details on doing so.

![microhard-overview](/images/userguides/microhard/microhard-overview.png)

## Requirements

### Hardware

The following hardware is required to establish a Microhard connection between VOXL and a host computer.

| Part Number | Description | Link |
| --- | --- | --- |
| VOXL-ACC-MH | VOXL Microhard Modem Add-on | [Purchase](https://www.modalai.com/collections/voxl-add-ons/products/voxl-microhard-modem-usb-hub)
| MH-SA | Microhard pDDL Carrier USB Dongle | [Purchase](https://www.modalai.com/products/mh-sa-1)

VOXL Microhard Modem Add-on             |  Microhard pDDL Carrier USB Dongle
:-------------------------:|:-------------------------:
![MH Add-on](/images/datasheet/microhard/voxl-acc-mh-assembled.jpg)  |  ![MH Standalone](/images/datasheet/microhard/standalone.jpg)

## Establishing Connection

### Hardware Setup

In order for the Microhard modules to communicate correctly, one module needs to be setup as a `Master` and the other needs to be setup as a `Slave`.

For the best performance, it is required to connect the `Master` to the source (e.g. VOXL, Drone, Video Source) and the `Slave` to the receiver (e.g. PC).

#### Hardware Setup - Master 

- Remove power and USB from the VOXL main board
- Attach an antenna to the `ANT` connection on the Microhard Modem
- Insert Microhard Modem into VOXL Microhard Modem Add-on, pin 1 should line up with the white dot on the board.
- Attach VOXL Microhard Modem Add-on to VOXL's `J13` Expansion B2B connector.
- Connect VOXL to Host PC via VOXL's `J8` Micro USB connector
- Power VOXL.

#### Hardware Setup - Slave 
- Attach an antenna to the `ANT` connection on the Microhard Modem
- Insert Microhard Modem into the Microhard pDDL Carrier USB Dongle, pin 1 should line up with the white dot on the board.
- Connect the Microhard USB Dongle to the host PC via the single `To USB Host` connector on the bottom of the board
- Power the board.

### Software Setup

#### Automated Software Setup

In order to establish a link between the Microhard Modems in a quick and easy way, we can take advantage of the VOXL Microhard Modem Add-on and Microhard pDDL Carrier USB Dongle's `S2` buttons, which act as "reset" buttons for the Microhard Modems.

Before starting, make sure both the VOXL and Microhard pDDL Carrier USB Dongle are powered by following the steps above.

1. Press and hold the `S2` button on the VOXL Microhard Add-on board for at least **10 seconds**, then release. The module will then reset all settings to the default values required for a **Master** module. This module will be assigned a default IP address of `192.168.168.1`

2. Press and hold the `S2` button on the Microhard pDDL Carrier USB Dongle for **5 seconds**, then release. The module will then reset all settings to the default values required for a **Slave** module. This module will be assigned a default IP address of `192.168.168.2`

#### Enabling Microhard Connection on Startup

[voxl-meta-package](https://gitlab.com/voxl-public/voxl-meta-package) is a repository containing setup scripts used for common vehicle configurations. The microhard meta-package can be used to configure the Microhard modem on bootup and gives the option to start an rtsp video stream.

Run this script from the root of this repository on your host PC with the VOXL connected via. USB.

Requirements:

- System Image: 2.2+
- Factory Bundle: 0.0.4+
- Software Bundle: 0.0.4+

Usage:

- Enable Microhard radio on VOXL startup with a static IP address specified by setup script, for example `192.168.168.100`

```bash
./microhard/setup.sh 192.168.168.100
```

Note: The chosen IP address must be of the form: `192.168.168.XXX`, where XXX is a 3 digit number

- As above, but and enable `voxl-rtsp` streaming from image sensor instance 0

```bash
./microhard/setup.sh 192.168.168.100 -r
```

```
# Reboot VOXL to start service
adb reboot
```

In order to prevent the VOXL from enabling Microhard connection on startup, disable the startup service on VOXL using:

```bash
# Disable startup service
systemctl disable microhard-init
```

You can then re-enable the service at a later time using:

```bash
# Re-enable startup service
systemctl enable microhard-init
```

#### Enabling Microhard Connection Manually

If you don't want the Microhard connection to start on bootup, you can initialize a connection using the following:

On VOXL, either via ADB or SSH, run the following command:

NOTE: The "--microhard_configure" option is only available with software bundles  0.0.6 or newer.

```
$ voxl-modem --microhard_configure=IP_ADDR

# Example: Set IP of VOXL to 192.168.168.100
$ voxl-modem --microhard_configure=192.168.168.100
```

You should, see an output similar to the following:

```
 # voxl-modem --microhard_configure=192.168.168.100
Setting up GPIO pins for the Microhard Modem
Attempting to obtain IP: 192.168.168.100
udhcpc (v1.23.2) started
Sending discover...
Sending discover...
Sending discover...
Sending discover...
Sending select for 192.168.168.100...
Lease of 192.168.168.100 obtained, lease time 43200
```

#### Manual Software Setup

If you would rather configure your Microhard Modems manually, you can follow the steps below to do so.

#### Software Setup - Slave
Microhard modules are shipped with a default static IP of `192.168.168.1` and will assign an IP to any connected device or computer with DHCP enabled. Therefore, unless the Microhard Modem has been previously configured, it should have an IP of `193.168.168.1`

- Test whether the host pc is able to communicate to the Microhard Modem on the VOXL by pinging it

```bash
# Microhard IP may be different if previously configured
$ ping 192.168.168.1
```

- If the ping was successful, navigate in your browser to the IP of the Microhard Modem (i.e. 192.168.168.1)
- If the connection was established correctly, you will be prompted for a Username and Password. Enter the factory defaults below:

```bash
Username: admin
Password: admin
```

- Once logged in for the first time, you will be forced to change this password
- Select `Network` from the top/main navigation bar
- Select `LAN` from the submenu
- Select `Edit` on LAN interface 1.
- Choose `Static IP` for the Connection Type.

For this example, we will use the following custom IP addresses for the Microhard modems:

```bash
Master: 192.168.168.11
Slave: 192.168.168.12
```

Feel free to use IP addresses of your choosing.
- Enter the following Network Information:

```
IP address: 192.168.168.12
IP Subnet Mask: 255.255.255.0
Default Gateway: 192.168.168.11
```

- Click submit to write the changes to the Microhard, once the IP address is changed, you will need to type the new address into your browswer to configure the configuration.
- Select `Wireless` from the top/main navigation bar.
- Select `RF` from the submenu

In the `RF Configuration` section, the Compatibility Mode, Channel Bandwidth, and Channel Frequency need to be set to the same on each module.

If you are setting up the Microhard connection for testing at your desk or close proximity operation, you will need to use a lower power setting to prevent RF saturation. Select a lower value from the `Tx Power` setting. The lowest setting (`7 dbm`) will work for bench testing.

- Select `Slave` from the `Operation Mode` dropdown
- Set a Network ID, which will need to be the same on each unit in the network. The default is "pDDL"
- For basic setup, the rest of the settingds can be left as defaults.
- Click on the `Submit` button to write the changes.

- You may now remove the VOXL's USB connection to the host PC.

Next, we will setup the Standalone Microhard Board (Master)



#### Software Setup - Master
- Test whether the host pc is able to communicate to the Microhard Modem on the VOXL by pinging it

```bash
# Microhard IP may be different if previously configured
$ ping 192.168.168.1
```

- If the ping was successful, navigate in your browser to the IP of the Microhard Modem (i.e. 192.168.168.1)
- If the connection was established correctly, you will be prompted for a Username and Password. Enter the factory defaults below:

```bash
Username: admin
Password: admin
```

- Once logged in for the first time, you will be forced to change this password
- Select `Network` from the top/main navigation bar
- Select `LAN` from the submenu
- Select `Edit` on LAN interface 1.
- Choose `Static IP` for the Connection Type.

For this example, we will use the following custom IP addresses for the Microhard modems:

```bash
Master: 192.168.168.11
Slave: 192.168.168.12
```

Feel free to use IP addresses of your choosing.


- Enter the following Network Information:

```
IP address: 192.168.168.11
IP Subnet Mask: 255.255.255.0
```

- Click submit to write the changes to the Microhard, once the IP address is changed, you will need to type the new address into your browswer to configure the configuration.
- Select `Wireless` from the top/main navigation bar.
- Select `RF` from the submenu

In the `RF Configuration` section, the Compatibility Mode, Channel Bandwidth, and Channel Frequency need to be set to the same as the previous module.

- Select `Master` from the `Operation Mode` dropdown
- Set the Network ID to the same as the Slave
- For basic setup, the rest of the settingds can be left as defaults.
- Click on the `Submit` button to write the changes.

To return to the configuration page, you will need to enter the device's new IP in the browser.

### Testing the Connection

At this point, the VOXL should be disconnected from the host PC and the Standalone board should be connected to the host pc. The devices should be attempting to connect to eachother.

- Visually check to see if the Microhard modems are communicating

The RSSI LEDs (green) represent strength, the more LEDs that are illuminated, the stronger the signal. If the devices are still searching for eachother, the LEDs will cycle through. For bench testing, all three lights will most likely be solid. 

If you navigate to the `Status` submenu from the `Wireless` option on the top/main navigation bar, you can see a `Connection Status` section which will list the connected devices to the corresponding Microhard modem.

- With the host PC connected to the Standalone board, type in the IP address of the `Slave` into the address bar of your browser. 

You should be able to connect, log in, and view the Web UI of the Slave via the wireless connection. If the `System Summary` screen is displayed correctlym the the connection is complete and successful!

### Connecting to QGroundControl

The VOXL system uses the `voxl-vision-px4` service on VOXL to establish a connection between PX4 and QGroundControl over UDP.  This is supported on VOXL Flight or when using Flight Core with VOXL (using [this procedure](/how-to-connect-voxl-to-flight-core/)).

#### Ground Control Station IP Address

Locate the IP address of the host PC that has been assigned to it by the Microhard modem. An example output is below:

```bash
$ ifconfig

enp1s0    Link encap:Ethernet  HWaddr 48:D7:05:EA:06:36  
          inet addr:192.168.168.135  Bcast:192.168.168.255  Mask:255.255.255.0
          inet6 addr: fe80::4ad7:5ff:feea:636/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:125 errors:0 dropped:0 overruns:0 frame:0
          TX packets:247 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:13399 (13.0 KiB)  TX bytes:31491 (30.7 KiB)
```

In this case, the IP address of the host PC is `192.168.168.135`

#### Configure Ground Control Station IP Address on VOXL

If you have not yet setup `voxl-vision-px4` on your hardware, do the following:

```bash
# On Host PC
$ adb shell

# On VOXL
yocto:/home/root# voxl-configure-vision-px4
```

You will be asked a variety of questions about the camera configuration (if any) and physical configuration of your drone (if any). Answer these questions with the options that best describe your setup, you can select no cameras as an option.

- When asked for the IP address for QGroundControl, you'll use `192.168.168.135` in this example

If you have already configured `voxl-vision-px4`, you can edit the configuration file as needed.  Run the following command and update the `qgc_ip` field to match the GCS computer's IP, in this example `192.168.168.135`

```bash
yocto:/home/root# vi /etc/modalai/voxl-vision-px4.conf
```

If you've modified the config file, reset the `voxl-vision-px4` service and reload the configuration by running the following command:

```bash
yocto:/home/root# systemctl restart voxl-vision-px4
```
