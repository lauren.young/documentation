---
layout: default
title: LTE Modem and USB Add-On Board Manual
parent: VOXL Add-Ons
nav_order: 1
permalink: /lte-modem-manual/
---

# LTE Modem and USB Add-On Board Manual
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

The VOXL system provides the ability to easily and quickly add an LTE connection.  The following guide provides you the necessary details on doing so.

## Requirements

### Hardware

In order to connect the VOXL to an LTE network, the VOXL-ACC-LTE accessory kit is required.  These can be purchased [here](https://www.modalai.com/4g-add-on) and are available in two options:

| Part Number | Description |
| --- | --- |
| VOXL-ACC-LTEH-DOD | Add-on board for VOXL which enables DOD spectrum LTE as well as adds a USB hub to integrate more peripherals |
| VOXL-ACC-LTEH-NA | Add-on board for VOXL which enables North American carrier LTE as well as adds a USB hub to integrate more peripherals CAT4 LTE Bands 2,4,5,12 |
| VOXL-ACC-LTEH-7607 | Add-on board for VOXL utilizing the [WP7607 module](https://www.sierrawireless.com/products-and-solutions/embedded-solutions/products/wp7607/) |
| VOXL-ACC-LTEH-7610 | Add-on board for VOXL utilizing the [WP7610 module](https://www.sierrawireless.com/products-and-solutions/embedded-solutions/products/wp7610/) |

![Modems](/images/userguides/modems.png)

Each kit conveniently comes with two LTE antennas (which are also available [here](https://www.digikey.com/product-detail/en/pulselarsen-antennas/W3907B0100/1837-1003-ND/7667474))

### Software

The following are required which should already be pre-installed:

- [System Image 2.3 or newer](/voxl-system-image/)
- The `voxl-utils` and `voxl-modem` packages

## SIM Cards and Supported Networks

The modems cards require a Nano SIM.

AT&T, Verizon and T-Mobile are supported.  For Band 3 support, the `VOXL-ACC-LTEH-DOD` modem is required and Band 3 SIM cards need to be requested through ModalAI.

## Hardware Setup

- Remove power and USB from the VOXL main board
- Insert SIM card into LTE card
- Connect antennas to the LTE card (note that it takes a little force to attach the U.FL connectors...)
- Attach LTE card to the VOXL's J13 connector.  3/8" #2-56 nylon screws and #2-56 nylon nuts can be used with a 5mm nylon spacer between the boards.
  - **Note:** The switch near the antenna connectors should be in the **'off'** position.  When 'on', it will force the main board to into USB boot mode
- Connect the USB cable to the VOXL main board
- Connect the power supply, you should see green LEDs like

![Modem-1](/images/userguides/modem-1.png)

## Software Setup

The VOXL ships with utilities on board to setting up the modem (System Image 2.3.0  or newer required).  First, initialize the modem:

```bash
voxl-modem --feather_configure
```

Next, we need to configure the APN. The following shows some sample APNs and how to implement them.

For IoT devices on the AT&T network:
```bash
set-apn.sh m2m.com.attz
```

For laptops and tablets on the AT&T network:
```bash
set-apn.sh broadband
```

For Smartphones on the AT&T network:
```bash
set-apn.sh phone
```

For LTE devices on the T-Mobile network:
```bash
set-apn.sh fast.t-mobile.com
```

**NOTE:** Use only one APN depending on your carrier and plan.

After issuing the last command, you can check for an `OK` response using the following:

```bash
cat /dev/ttyACM0
(CTRL+C)
```

Reboot the device to finish configuration.

```bash
shutdown -r now
```

After rebooting, connect via ADB again and then connect to the network using the following command:

```bash
voxl-modem --connect_to_network
```

If successful, you should see the WWAN status LED illuminate. This indicates that
the modem has successfully registered with the network. You will also see a
new network interface `rmnet_usb0`:

```bash
/ # ifconfig
lo        Link encap:Local Loopback
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:48 errors:0 dropped:0 overruns:0 frame:0
          TX packets:48 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0
          RX bytes:3504 (3.4 KiB)  TX bytes:3504 (3.4 KiB)

rmnet_usb0 Link encap:UNSPEC  HWaddr 00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00
          inet addr:10.39.193.86  Mask:255.255.255.252
          UP RUNNING  MTU:2000  Metric:1
          RX packets:2 errors:0 dropped:0 overruns:0 frame:0
          TX packets:8 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:612 (612.0 B)  TX bytes:1112 (1.0 KiB)

wlan0     Link encap:Ethernet  HWaddr 48:5F:99:9D:55:DF
          inet addr:192.168.8.1  Bcast:192.168.8.255  Mask:255.255.255.0
          inet6 addr: fe80::4a5f:99ff:fe9d:55df/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:11 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:3000
          RX bytes:0 (0.0 B)  TX bytes:858 (858.0 B)
```

![Modem-3](/images/userguides/modem-2.png)

## Enabling LTE connection on startup

[voxl-meta-package](https://gitlab.com/voxl-public/voxl-meta-package) is a repository containing setup scripts used for common vehicle configurations. The LTE meta-package can be used to configure VOXL to enable the LTE service on bootup.

[v1 Modem](https://docs.modalai.com/lte-modem-and-usb-add-on-datasheet/) Requirements:

- System Image: 2.2+

- Factory Bundle: 0.0.4+

- Software Bundle: 0.04+

[v2 Modem](https://docs.modalai.com/lte-modem-and-usb-add-on-v2-datasheet/) Requirements:

- System Image: 2.3+

- Factory Bundle: 1.0.1+

- Voxl Suite

Usage:

Run this script from the lte directory of the voxl-meta-package repository on your host PC with the VOXL connected via. USB.

```bash
cd lte

# Enable LTE radio on VOXL startup
./setup.sh
```

In order to prevent the VOXL from enabling LTE connection on startup, disable the startup service on VOXL using:

```bash
# Disable LTE startup service 
systemctl disable cellular-start
```

You can then re-enable the service at a later time using:

```bash
# Re-enable LTE startup service
systemctl enable cellular-start
```

## Connecting to a Ground Control Station

A typical use case of an LTE connection is to connect the drone with a Ground Control
Station (GCS) such as [QGroundControl](http://qgroundcontrol.com/). Typically the GCS is
also connected to an LTE network. In this case both the drone and the GCS have been
assigned IP addresses on the LTE network that are private to the carrier network. This
prevents the drone from directly connecting to the GCS since their IP addresses are
not visible to each other over the Internet. There are a variety of ways of solving this
issue. ModalAI uses a VPN as the preferred solution.

![LTE data flow](/images/userguides/lte-to-lte-data-flow.png)

This diagram shows how the VPN solution works. In this example a server is allocated with
static IP address 35.236.55.229. You can easily set your own server up with Google
Cloud Platform, Amazon Web Services, Microsoft Azure, etc. Ubuntu is a nice choice
of OS for your server. Our VPN is setup using the [OpenVPN software package](https://openvpn.net/)
which is nicely documented.

In the example, once the drone has connected to the AT&T network it obtains address 10.47.x.y and the
GCS, once connected, obtains the address 10.47.x.z. With the OpenVPN server software
running on the cloud server, OpenVPN client software on both the drone and the GCS
computer can now connect and get a VPN IP address assignment. In this diagram the drone
is assigned 10.8.0.6 and the GCS computer is assigned 10.8.0.8. The drone can now
communicate directly to the GCS computer using the 10.8.0.8 address.

It is desirable for the drone and the GCS to always get the same IP address when
connecting to the VPN. This is possible by assigning each separate network endpoint
a unique security certificate. When each endpoint connects using it's certificate it can
be configured to receive the same address every time. This is explained in more
detail in the OpenVPN documentation.

### OpenVPN client on VOXL

The base VOXL software image already has OpenVPN installed. To use it you need to supply
a configuration file and the certificates / keys for your VPN. These files can be
placed in the directory /etc/openvpn. Here is a example of running OpenVPN as a daemon:

```bash
cd /etc/openvpn

openvpn --daemon --script-security 2 --config target-client.conf
```

Note that the connection will fail unless the date and time have been correctly
set. This is required due to the dates embedded in the certificates.

The default version of OpenVPN on VOXL is 2.1.3. Newer versions are available and
compile nicely on target. Here is an example of setting up version 2.4.6:

```bash
git clone https://github.com/OpenVPN/openvpn

cd openvpn
git checkout tags/v2.4.6

autoreconf -i -v -f

./configure --disable-lzo --disable-plugin-auth-pam --prefix=/usr/local

make -j4

make install
```

An example client configuration file is attached to the end of this user manual.

## FCC Restriction on Band 5

When using cellular communications on any flying platform the use of band 5 is strictly
prohibited by the FCC. In order to abide by this restriction the modem must be configured
to use LTE only and then configured to use all available LTE bands supported by
the hardware except for band 5. ModalAI provides an application called
"configband" to configure this properly. The configband application takes a bitmask
as one of its arguments. The bitmask represents the allowable LTE bands. For the
WNC North American modem, which supports bands 2, 4, 5, and 12, the bitmask would be
0x80a (100000001010b) to only enable bands 2, 4, and 12. Here is an example of setting
up the modem to disable band 5 and then connecting to the network:

```bash
voxl-modem --feather_configure

uqmi -P -d /dev/qcqmi0 --set-network-preference lte

configband write /dev/ttyUSB0 0x80a

voxl-modem --connect_to_network
```

## Example VOXL OpenVPN client configuration file

```
##############################################
# Sample client-side OpenVPN 2.0 config file #
# for connecting to multi-client server.     #
#                                            #
# This configuration can be used by multiple #
# clients, however each client should have   #
# its own cert and key files.                #
#                                            #
# On Windows, you might want to rename this  #
# file so it has a .ovpn extension           #
##############################################

# Specify that we are a client and that we
# will be pulling certain config file directives
# from the server.
client

# Use the same setting as you are using on
# the server.
# On most systems, the VPN will not function
# unless you partially or fully disable
# the firewall for the TUN/TAP interface.
;dev tap
dev tun

# Windows needs the TAP-Win32 adapter name
# from the Network Connections panel
# if you have more than one.  On XP SP2,
# you may need to disable the firewall
# for the TAP adapter.
;dev-node MyTap

# Are we connecting to a TCP or
# UDP server?  Use the same setting as
# on the server.
;proto tcp
proto udp

# The hostname/IP and port of the server.
# You can have multiple remote entries
# to load balance between the servers.
remote <Your server name or IP here> 1194
;remote my-server-2 1194

# Choose a random host from the remote
# list for load-balancing.  Otherwise
# try hosts in the order specified.
;remote-random

# Keep trying indefinitely to resolve the
# host name of the OpenVPN server.  Very useful
# on machines which are not permanently connected
# to the internet such as laptops.
resolv-retry infinite

# Most clients don't need to bind to
# a specific local port number.
nobind

# Downgrade privileges after initialization (non-Windows only)
user nobody
group nobody

# Try to preserve some state across restarts.
persist-key
persist-tun

# If you are connecting through an
# HTTP proxy to reach the actual OpenVPN
# server, put the proxy server/IP and
# port number here.  See the man page
# if your proxy server requires
# authentication.
;http-proxy-retry # retry on connection failures
;http-proxy [proxy server] [proxy port #]

# Wireless networks often produce a lot
# of duplicate packets.  Set this flag
# to silence duplicate packet warnings.
;mute-replay-warnings

# SSL/TLS parms.
# See the server config file for more
# description.  It's best to use
# a separate .crt/.key file pair
# for each client.  A single ca
# file can be used for all clients.
ca ca.crt
cert example.crt
key example.key

# Verify server certificate by checking that the
# certicate has the correct key usage set.
# This is an important precaution to protect against
# a potential attack discussed here:
#  http://openvpn.net/howto.html#mitm
#
# To use this feature, you will need to generate
# your server certificates with the keyUsage set to
#   digitalSignature, keyEncipherment
# and the extendedKeyUsage to
#   serverAuth
# EasyRSA can do this for you.
remote-cert-tls server

# If a tls-auth key is used on the server
# then every client must also have the key.
;tls-auth ta.key 1

# Select a cryptographic cipher.
# If the cipher option is used on the server
# then you must also specify it here.
# Note that v2.4 client/server will automatically
# negotiate AES-256-GCM in TLS mode.
# See also the ncp-cipher option in the manpage
;cipher AES-256-CBC

# Enable compression on the VPN link.
# Don't enable this unless it is also
# enabled in the server config file.
#comp-lzo

# Set log file verbosity.
verb 3

# Silence repeating messages
;mute 20

keepalive 10 120

tun-mtu 1300
mssfix 1260

writepid /run/tun.pid

askpass passphrase.txt
```
