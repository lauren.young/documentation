---
layout: default
title: VOXL Fan Control
parent: VOXL IO Guides
grand_parent: Developing with VOXL
nav_order: 3
permalink: /voxl-fan-control/
---

# How to Control Fan on VOXL
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Connect the Fan
Connect the cooling fan rated for 5V to J6 (see [VOXL Connectors](/voxl-datasheet-connectors/))

## Control the Fan
```
# Spin the fan using 25kHz PWM, 50% duty cycle
PWM_DEVICE=/sys/class/pwm/pwmchip0
echo 0 >     ${PWM_DEVICE}/export
echo 40000 > ${PWM_DEVICE}/pwm0/period
echo 20000 > ${PWM_DEVICE}/pwm0/duty_cycle
echo 1 >     ${PWM_DEVICE}/pwm0/enable
```

Notes
- The fan turns on at full power when the board is turned on
- The ```period``` and ```duty_cycle``` numbers are in microseconds
- PWM Switching Frequency (kHz) = 1000000 / ```period```
- PWM ```duty_cycle``` should be in the range of [0..```period```]
- Only several discrete frequencies are possible : 12.5khz, 15.00khz 18.75khz, 25khz, 30.00khz, 37.5khz, 50khz
- The requested period gets rounded off or truncated to pick one of these frequencies
