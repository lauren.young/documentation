---
layout: default
title: Build Environments
parent: Developing with VOXL
nav_order: 1
has_children: true
permalink: /build-environments/
---

# Build Environments

The [voxl-docker](https://gitlab.com/voxl-public/voxl-docker) project provides setup instructions for two docker images which provide build environments for the VOXL's ARM applications processor and Hexagon SDSP. It also provides the "voxl-docker" script for easily launching these docker images. These two docker images are prerequisites for building the majority of open-source projects on https://gitlab.com/voxl-public

See [Installing voxl-docker](/install-voxl-docker/) for instructions on installing and using these docker images.


## voxl-emulator for building applications-processor projects

We provide a Docker image in .tar format which contains the root file system that runs on VOXL itself. Through QEMU emulation and Docker, the ARM binaries in the VOXL rootFS can run on a desktop computer aiding development and speeding up compilation time. Anything that can be compiled onboard VOXL should be able to be compiled in voxl-emulator.

As of voxl-emulator:V1.1 factory and software bundles V0.0.3 are pre-installed which should cover most build dependencies.

## voxl-hexagon for building hexagon DSP projects

The voxl-hexagon docker image is based on the x86_64 Ubuntu Bionic docker image but additionally contains the Qualcomm Hexagon SDK 3.1 and an ARM cross compiler. For legal reasons these components must be downloaded from their respective sources by the user before building the docker image. However, we provide instructions and an install script here in this project.

## voxl-cross64 for building ARMv8 applications-processor projects

The voxl-cross64 Docker is configured with the proper cross-compiler to compile ARMv8 (64-bit) code for the VOXL platform. Some support libraries for the VOXL's hardware accelerators are not yet available, but it is on ModalAI's roadmap to fully support ARMv8 on VOXL.