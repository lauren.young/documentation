---
layout: default
title: Camera Connections
parent: Camera and Video Guides
grand_parent: Developing with VOXL
nav_order: 1
permalink: /camera-connections/
---

# Camera Connections
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Camera Ports

VOXL has 3 camera connectors Labeled J2, J3, J4 in the two images below.

![VOXL Core Connected](/images/datasheet/voxl-core-bottom.png)

*Image 1 - VOXL Bottom View*

![VOXL Core USB](/images/datasheet/voxl-core-top.png)

*Image 2 - VOXL Top View*

The connectors are attached to the following Camera Subsystem ports and have the following colloquial names. Note that the ports can be used in other configurations as described bellow, not just the purpose described by the port's colloquial name.

| Port  | Camera SS Port    | Colloquial Name               |
|------ |----------------   |------------------------------ |
| J2    | CSI0              | Hires Camera Port             |
| J3    | CSI1              | Stereo Camera Port            |
| J4    | CSI2              | Tracking Camera Port          |

## Configurations

For each of these configurations, the cameras are accessible in software via their respective Camera ID number. This is the number which should be set in the ROS nodes, camera_test app, and snav configurations as described in section 4.


### 1 Tracking + Stereo

This is the default configuration that uses both tracking and stereo cameras provided by the VOXL-DK-MV kit.

| Port  | Camera                | Camera ID |
|------ |---------------------- |-----------|
| J2    | Empty                 | N/A       |
| J3    | Stereo Cameras        | 1         |
| J4    | Tracking Camera       | 0         |

### 2 Tracking Only

| Port  | Camera                | Camera ID |
|------ |---------------------- |-----------|
| J2    | Empty                 | N/A       |
| J3    | Empty                 | N/A       |
| J4    | Tracking Camera       | 0         |


### 3 Hires + Stereo + Tracking

| Port  | Camera                | Camera ID |
|------ |---------------------- |-----------|
| J2    | Hires Camera          | 0         |
| J3    | Stereo Cameras        | 2         |
| J4    | Tracking Camera       | 1         |


### 4 Hires + Tracking

| Port  | Camera                | Camera ID |
|------ |---------------------- |-----------|
| J2    | Hires Camera          | 0         |
| J3    | Empty                 | N/A       |
| J4    | Tracking Camera       | 1         |

### 5 TOF + Tracking

| Port  | Camera                | Camera ID |
|------ |-----------------------|-----------|
| J2    | empty                 | N/A       |
| J3    | TOF time-of-flight    | 0         |
| J4    | Tracking Camera       | 1         |

### 6 Hires + TOF + Tracking

| Port  | Camera                | Camera ID |
|------ |-----------------------|-----------|
| J2    | Hires Camera          | 0         |
| J3    | TOF time-of-flight    | 1         |
| J4    | Tracking Camera       | 2         |


### 7 TOF + Stereo + Tracking

Not yet validated, work in progress!

| Port  | Camera                | Camera ID |
|------ |-----------------------|-----------|
| J2    | TOF time-of-flight    | 2         |
| J3    | Stereo Cameras        | 1         |
| J4    | Tracking Camera       | 0         |



## Configuration Files

Rarely, if ever, will you need to touch these configuration files manually. In most cases you can use the voxl-configure-cameras script as described above.

### System camera_config.xml

The primary camera configuration file which tells the Camera Subsystem what what connected to which port is located at  **/system/etc/camera/camera_config.xml**

This file should not be touched unless you are connecting cameras in a configuration not listed above or you really know what you are doing.

### Snav and ModalAI Vision Lib

SNAV and the ModalAI vision lib need to know which camera_id to use for VIO tracking. This is set in **/etc/snav/camera.downward.xml** with the "override_cam_id" parameter. eg:

```xml
<SnavCamera>
  <SnavCameraDownward>
    <param name="override_cam_id" value="0"/>
```

They also need to know which camera_id to use for the stereo pair for visual obstacle avoidance. This is set in **/etc/snav/camera.stereo.xml**

```xml
<SnavCamera>
  <SnavCameraStereo>
    <param name="override_cam_id" value="1"/>
```

### ROS

ROS launch files for voxl_cam_ros and tof_cam_ros pick the camera_id to use based on the following environment variables

```bash
HIRES_CAM_ID
TRACKING_CAM_ID
STEREO_CAM_ID
TOF_CAM_ID
```

These are loaded as bash environment variables by **/etc/modalai/camera_env.sh** which is called when starting a bash session by  /home/root/.bashrc

You do not need to set this file manually, like the others it is set up by voxl-configure-cameras.

## Compatible Cameras

This is an incomplete list, more to be added soon.

| Package (sensor)                                         | Resolution / FOV       | Description          | Minimum System Image |
|:---------------------------------------------------------|:-----------------------|:---------------------|:---------------------|
| [ModalAI M0024](https://modalai.com/M0024) (Sony IMX214) | 4k                     | Hires                | 2.0                  |
| [ModalAI M0025](https://modalai.com/M0025) (Sony IMX214) | 4k                     | Hires                | 2.0                  |
| [ModalAI M0026](https://modalai.com/M0026) (Sony IMX377) | 4k                     | Hires                | 2.0                  |
| Sony IMX412                                              | 4k                     | Hires                | 2.5.1                |
| [ModalAI Stereo Sensor](https://modalai.com/stereo-sensor) (OV7251)| 640x480 85.6º diagonal | Stereo               | 2.0                  |
| [ModalAI M0014](https://modalai.com/tracking-sensor) (OV7251)| 640x480 166º diagonal  | Tracking               | 2.0                  |
| Sunny GP161C (OV7251)                                    | 640x480 85.6º diagonal | Stereo               | 2.0                  |
| Sunny MD102A (OV7251)                                    | 640x480 166º diagonal  | Tracking             | 2.0                  |
| LiteOn A65 (irs10x0c)                                    | 224x172 110º horizontal| TOF (Time of Flight) | 2.3                  |         


