---
layout: default
title: Camera and Video Guides
parent: Developing with VOXL
nav_order: 3
has_children: true
permalink: /camera-video-guides/
---

# Camera and Video Guides

This section contains instructions on how to use VOXL's camera, computer vision, and video features.

## Supported Cameras

The VOXL architecture (VOXL and VOXL Flight) support the following interfaces for cameras and image sensors

| Interface | Types of Cameras | Information |
| --- | --- | --- |
| MIPI (Direct connection to VOXL/VOXL Flight PCB) | Image sensors such as IMX377 and OV7251. Depth imagers such as PMD Time of Flight (TOF) | [Datasheets](https://docs.modalai.com/voxl-image-sensor-datasheets/) |
| USB (UVC) | Webcams | [Instructions](https://docs.modalai.com/uvc-streaming/) | 
| USB (libgphoto2) | Hundreds of DSLR Cameras | [Instructions](https://docs.modalai.com/voxl-libgphoto2/) |
| HDMI | | Coming soon |
| Flir Thermal Imaging | Boson | Coming Soon |

### VOXL MIPI-CSI2 Supported configurations

The following table conveys the camera sensor configurations currently supported on VOXL directly through MIPI CSI-2. Additional cameras can be supported through UVC (Linux USB Camera). 

| Configuration | Sensor Interface 0 (CSI0) | Sensor Interface 1 (CSI1) | Sensor Interface 2 (CSI2) | 
| --- | --- | --- | --- |
| 1 | [High-res](https://docs.modalai.com/voxl-hires-camera-datasheet/) 4k RGB rolling shutter camera 30Hz (IMX214, IMX296, IMX377, or IMX412) | [Stereo](https://docs.modalai.com/voxl-stereo-camera-datasheet/) Synchronized  Stereo Sensor Pair B&W (OV7251) | [Tracking](https://docs.modalai.com/voxl-tracking-camera-datasheet/) VGA B&W global shutter up to 90Hz (OV7251) |
| 2 | [High-res](https://docs.modalai.com/voxl-hires-camera-datasheet/) 4k RGB rolling shutter camera 30Hz (IMX214, IMX296, IMX377, or IMX412) | [ToF](https://docs.modalai.com/voxl-tof-sensor-datasheet/) Active Time of Flight Sensor | [Tracking](https://docs.modalai.com/voxl-tracking-camera-datasheet/) VGA B&W global shutter up to 90Hz (OV7251) |
| 3 | [Tracking](https://docs.modalai.com/voxl-tracking-camera-datasheet/) VGA B&W global shutter up to 90Hz (OV7251) | [Stereo](https://docs.modalai.com/voxl-stereo-camera-datasheet/) Synchronized  Stereo Sensor Pair B&W (OV7251) | [Tracking](https://docs.modalai.com/voxl-tracking-camera-datasheet/) VGA B&W global shutter up to 90Hz (OV7251) |
| 4 | [Tracking](https://docs.modalai.com/voxl-tracking-camera-datasheet/) VGA B&W global shutter up to 90Hz (OV7251) | [ToF](https://docs.modalai.com/voxl-tof-sensor-datasheet/) Active Time of Flight Sensor | [Tracking](https://docs.modalai.com/voxl-tracking-camera-datasheet/) VGA B&W global shutter up to 90Hz (OV7251) |

## Stream and Retrieve Video and Images

| Method | Information |
| --- | --- |
| ROS | [Setup](https://docs.modalai.com/setup-ros-on-voxl/) [voxl-cam-ros node](https://docs.modalai.com/voxl-cam-ros/) |
| RTSP | [UVC](https://docs.modalai.com/uvc-streaming/) or [MIPI](https://docs.modalai.com/voxl-rtsp/) |
| Image Streamer | [Instructions](https://docs.modalai.com/image-streamer/) |

