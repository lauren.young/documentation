---
layout: default
title: Use TensorFlow Lite on VOXL
parent: Build Environments
grand_parent: Developing with VOXL
nav_order: 5
permalink: /tflite/
---

# How to use TensorFlow Lite on VOXL

We have developed an OpenCL GPU-accelerated TensorFlow Lite for ARM Linux and made it available for VOXL. 

Please see example code details [here](https://gitlab.com/voxl-public/apps-proc-examples/-/tree/master/hellotflitegpu)

A video demonstration of TensorFlow Lite running in real-time on the VOXL platform follows

<iframe width="560" height="315" src="https://www.youtube.com/embed/_vTosYL6050" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>