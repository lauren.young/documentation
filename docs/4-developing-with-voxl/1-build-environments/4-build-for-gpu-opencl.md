---
layout: default
title: Build for GPU (OpenCL)
parent: Build Environments
grand_parent: Developing with VOXL
nav_order: 4
permalink: /build-for-gpu-opencl/
---

# Build for GPU (OpenCL)

## Overview

TODO

## Examples


Examples of how to write code for the Qualcomm® Adreno™ 530 GPU

| Name | Description | Link |
| --- | --- | --- |
| Hello CL | Simple application showing how to interact with the GPU using OpenCL | [GitLab](https://gitlab.com/voxl-public/apps-proc-examples/tree/master/hellocl) |

