---
layout: default
title: Build for Hexagon Sensors DSP (sDSP)
parent: Build Environments
grand_parent: Developing with VOXL
nav_order: 3
permalink: /build-for-sdsp/
---

# Build for Hexagon Sensors DSP (sDSP)

## Prerequisite: voxl-hexagon docker image

Follow the instructions to install voxl-docker and the voxl-hexagon docker image [https://docs.modalai.com/install-voxl-docker/](here).

## Overview

There are two DSPs available for programming on VOXL (Snapdragon 821). The Sensors DSP (sDSP) and the Applications DSP ([sDSP](https://docs.modalai.com/build-for-adsp/))

The sDSP is a real-time digital signal processor to offload real-time I/O tasks from the CPU. A flight controller, for instance, can be run on this DSP.

The aDSP is well suited for computer vision, fixed-point offloading tasks.

## Examples

Examples of how to write code for the Qualcomm® Hexagon™ Sensors Low Power Island:

| Name        | Description                                                                | Link                                                                               |
|:------------|:---------------------------------------------------------------------------|:-----------------------------------------------------------------------------------|
| Hello World | Simple hello world application to build and run code on the sDSP processor | [GitLab](https://gitlab.com/voxl-public/sdsp-proc-examples/tree/master/helloworld) |
| libvoxl-io  | Talk to serial ports through the sDSP                                      | [GitLab](https://gitlab.com/voxl-public/libvoxl_io)                                |

## Tools

### mini-dm

mini-dm is a diag log tool for debugging the Hexagon DSP that comes with [Hexagon SDK](https://developer.qualcomm.com/software/hexagon-dsp-sdk).

To configure on Ubuntu:

```lsusb``` and find the Qualcomm entry.
VOXL's device ID is
```05c6:901d Qualcomm, Inc.```

Edit /etc/udev/rules.d/70-android.rules and add the following line:
```SUBSYSTEM=="usb",ATTRS{idVendor}=="05c6",ATTRS{idProduct}=="901d",MODE=="0666"```

To run:

```$ ./Qualcomm/Hexagon_SDK/3.3.3/tools/debug/mini-dm/Linux_Debug/mini-dm```