---
layout: default
title: Build for Applications Processor (ARM CPU)
parent: Build Environments
grand_parent: Developing with VOXL
nav_order: 2
permalink: /build-for-apps-proc/
---


# Build for Applications Processor (ARM CPU)


VOXL uses a Snapdragon 821 with an ARMv8-A 64-bit Qualcomm® Kyro Quad Core CPU.

The following are good examples of how to build applications processor (apps-proc) code with the voxl-emulator docker image:


| Name            | Description                                           | Link                                                       |
|:----------------|:------------------------------------------------------|:-----------------------------------------------------------|
| librc_math      | Example of linear algebra routines in C               | [Source](https://gitlab.com/voxl-public/librc_math)        |
| OpenCV 3.4.6    | How to build OpenCV 3.4.6 for VOXL, with examples     | [Source](https://gitlab.com/voxl-public/voxl-opencv-3-4-6) |
| voxl-vision-px4 | Example use of ModalAI Vision Library for VIO and VOA | [Source](https://gitlab.com/voxl-public/voxl-vision-px4)   |




## Build for 32-bit using voxl-emulator docker (Recommended)

The [https://docs.modalai.com/install-voxl-docker/](voxl-emulator) builds 32-bit applications and is easy to use for quick ports of existing applications.

Much of the base Yocto image uses 32-bit libraries for backwards compatibility. We are enabling many of the components and migrating examples to 64-bit.

The system image is built using GCC4.9 so most applications are required to be built with that same version of compiler.

Our recommended compiler flags for 32-bit are as follows:

```
-std=c++11 -march=armv7-a -mfloat-abi=softfp -mfpu=neon-vfpv4
```

A good example of that can be found [https://gitlab.com/voxl-public/voxl-opencv-3-4-6/blob/master/package/build.sh](here)

### 64-bit

The [https://gitlab.com/voxl-public/voxl-docker/tree/master/voxl-cross64](voxl-cross64) Docker can be used to cross-compile 64-bit applications using GCC 4.9

Our recommended compiler flags for 64-bit are as follows:

```
-std=c++11 -march=armv8-a
```

A good example of that can be found [https://gitlab.com/voxl-public/voxl-opencv-3-4-6/blob/master/package/build64.sh](here)