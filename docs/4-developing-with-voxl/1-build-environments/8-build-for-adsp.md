---
layout: default
title: Build for Hexagon Applications DSP (aDSP)
parent: Build Environments
grand_parent: Developing with VOXL
nav_order: 8
permalink: /build-for-adsp/
---

# Build for Hexagon Applications DSP (aDSP)

## Overview

There are two DSPs available for programming on VOXL (Snapdragon 821). The Applications DSP (aDSP) and the Sensors DSP ([sDSP](https://docs.modalai.com/build-for-sdsp/))

The aDSP is a powerful digital signal processor to offload signal processing tasks from the CPU. It is most capable performing fixed-point computer vision compute offload.

The sDSP is well suited to work with I/O and has a real-time operating system QuRT.

## Examples

Examples of how to write code for the Applications DSP, Qualcomm® Hexagon™ 680 with HVX:

| Name        | Description                                                                | Link                                                                               |
|:------------|:---------------------------------------------------------------------------|:-----------------------------------------------------------------------------------|
| version-test | Simple application to grab information from the aDSP | [GitLab](https://gitlab.com/voxl-public/adsp-proc-examples/tree/master/version-test) |

