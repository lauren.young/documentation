---
layout: default
title: Use OpenCV on VOXL
parent: Build Environments
grand_parent: Developing with VOXL
nav_order: 5
permalink: /opencv/
---

# How to use OpenCV on VOXL


## Examples


OpenCV 3.4.6: [Download](https://storage.googleapis.com/modalai_public/modal_packages/latest/opencv_3.4.6_8x96.ipk), and [GitLab Source](https://gitlab.com/voxl-public/voxl-opencv-3-4-6).