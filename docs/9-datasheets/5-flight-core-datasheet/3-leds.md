---
layout: default
title: Flight Core LEDs
parent: Flight Core Datasheet
grand_parent: Datasheets
nav_order: 3
permalink: /flight-core-datasheet-leds/
---

# Flight Core LEDs
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---


![FC LEDs](../../../images/datasheet/fc-overlay-led-144-dpi.png)

### LED1

LED1 is marked on silkscreen as "DS1".  It's  tri-color RGB LED.

#### Bootloader Mode

Upon power up, while attached to USB, the device will stay in the bootloader mode for 5 seconds.

| Color | Pattern | Status |
| --- | --- | --- |
| Green | Fast Flashing (~10 Hz) | In bootloader, waiting |
| Yellow/Red | Alternating | Serial FW update in progress |

#### Normal Mode

| Color | Pattern | Status |
| --- | --- | --- |
| Green | Solid | Powered |
| Blue | Flashing (~2 Hz)| Disarmed |
| Blue | Solid | Armed |
| Red | Solid | Error, hard fault |

#### Sensor Calibration Mode

| Color | Pattern | Status |
| --- | --- | --- |
| Green | Solid | Powered |
| Blue | Flashing (~20 Hz)| Calibration in progress |

### LED2

LED2 is marked on silkscreen as "DS2".  It's  tri-color RGB LED.  **For future use**.

