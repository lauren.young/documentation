---
layout: default
title: Power Module Datasheets
parent: Datasheets
nav_order: 8
has_children: true
permalink: /power-module-datasheets/
---

# Power Module Datasheets
{: .no_toc }

ModalAI offers high performance 5V power adapters for drones (sUAS, UAVs) and other robotic use cases. These power modules support voltage and current monitoring for estimating battery consumption and remaining charge.

The v3 power monitor supports 2S-6S batteries, but does not support hot swapping.

The v2 power monitor supports 2S-5S batteries and hot-swapping batteries with a wall power supply.

{:toc}
