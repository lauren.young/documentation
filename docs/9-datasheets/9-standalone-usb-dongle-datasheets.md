---
layout: default
title: Standalone USB Dongle Datasheets
parent: Datasheets
nav_order: 9
has_children: true
permalink: /standalone-usb-dongle-datasheets/
---

# Standalone USB Dongle Datasheets
{: .no_toc }

Quickly add LTE or Microhard modems via USB with ModalAI's convenient standalone USB dongles.  Whether adding additional modems to VOXL or enabling new wireless capabilities to your laptop, these dongles are an easy way extend your range.

{:toc}
