---
layout: default
title: VOXL Stereo Sensor Datasheet
parent: VOXL Image Sensor Datasheets
grand_parent: Datasheets
nav_order: 3
has_children: false
permalink: /voxl-stereo-camera-datasheet/
---

# VOXL Stereo Sensor Datasheet
{: .no_toc }

## Specification

| Specicifcation | Value |
| --- | --- |
| Sensor | OV7251 [Datasheet](https://www.ovt.com/download/sensorpdf/146/OmniVision_OV7251.pdf) |
| Shutter | Global |
| Resolution | 640x480 |
| Framerate | up to 120Hz |
| Data formats | B&W 8 and 10-bit |
| Lens Size | 1/7.5" |
| Focusing Range | 5cm~infinity |
| Focal Length | 1.77mm |
| F Number | 2.5 |
| Fov(DxHxV) | 85° x 68° x 56° |
| TV Distortion | < -3.5% |
