---
layout: default
title: VOXL Hires Sensor Datasheet
parent: VOXL Image Sensor Datasheets
grand_parent: Datasheets
nav_order: 2
has_children: false
permalink: /voxl-hires-camera-datasheet/
---

# VOXL Hires Sensor Datasheet
{: .no_toc }

## Specification

### M0024 10cm IMX214 100° FOV  ([Buy Here](https://www.modalai.com/M0024))

| Specicifcation | Value |
| --- | --- |
| Sensor | IMX214 [Datasheet](https://www.mouser.com/datasheet/2/897/ProductBrief_IMX214_20150428-1289331.pdf) |
| Shutter | Rolling |
| Resolution | 4224x3200 |
| Framerate | up to 60Hz |
| Lens Size | 1/3.06" |
| Focusing Range | 5cm~infinity |
| Focal Length | 3.33mm |
| F Number | 2.75 |
| Fov(DxHxV) | ~100° |
| TV Distortion | < 6% |
| Weight | 3g |

### M0025 8.5cm IMX214 100° FOV ([Buy Here](https://www.modalai.com/M0025))

| Specicifcation | Value |
| --- | --- |
| Sensor | IMX214 [Datasheet](https://www.mouser.com/datasheet/2/897/ProductBrief_IMX214_20150428-1289331.pdf) |
| Shutter | Rolling |
| Resolution | 4224x3200 |
| Framerate | up to 60Hz |
| Lens Size | 1/3.06" |
| Focusing Range | 5cm~infinity |
| Focal Length |  |
| F Number |   |
| Fov(DxHxV) | ~100° |
| TV Distortion |   |
| Weight | <1g |

### M0026 17cm IMX377 100° FOV ([Buy Here](https://www.modalai.com/M0026))

| Specicifcation | Value |
| --- | --- |
| Sensor | IMX377 [Datasheet](https://pdf1.alldatasheet.com/datasheet-pdf/view/1150455/SONY/IMX377.html) |
| Shutter | Rolling |
| Resolution | 4000x3000 |
| Framerate | up to 60Hz |
| Lens Size | 1/2.3" |
| Focusing Range | 50cm~infinity |
| Focal Length | 3.24mm |
| F Number | 2.0  |
| Fov(DxHxV) | ~100° |
| TV Distortion |   |
| Weight | 10g |

## Module Connector Schematic for J2

![voxl-schematic-for-camera-module-to-connect-to-J2.png](../../images/datasheet/voxl-schematic-for-camera-module-to-connect-to-J2.png)
