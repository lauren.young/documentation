---
layout: default
title: VOXL m500 Functional Description
parent: VOXL m500 Reference Drone Datasheet
grand_parent: Datasheets
nav_order: 1
permalink: /voxl-m500-functional-description/
---

# VOXL m500 Snapdragon Reference Drone Functional Description
{: .no_toc }

[User Guide Here](/voxl-m500-getting-started/)

[Buy Here](https://shop.modalai.com/products/voxl-m500-r1)

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

![voxl-m500](../images/datasheet/m500/m500-shopify.jpg)

## High-Level Specs

| Specicifcation | [VOXL-M500-DK-R2-1](https://www.modalai.com/products/voxl-m500-r1?variant=31790290599987) | [VOXL-M500-DK-R2-G1-1](https://www.modalai.com/products/voxl-m500-r1?variant=31793533845555) |
| --- | --- | --- |
| Take-off Weight | 1075g with battery (820g w/o battery) | TBD |
| Flight Time | >20 minutes | TBD |
| Payload Capacity (Impacts Flight Time) | 1kg | TBD |
| Motors | [2216 880KV](https://shop.holybro.com/motor2216-880kv-1pc_p1154.html) | [2216 880KV](https://shop.holybro.com/motor2216-880kv-1pc_p1154.html) |
| Propellers | 10" (1045) | 10" (1045) |
| ESCs* | [BLHeli 20A](https://shop.holybro.com/blheli-esc-20a_p1143.html) | [BLHeli 20A](https://shop.holybro.com/blheli-esc-20a_p1143.html) |
| Computing | [VOXL Flight](/voxl-flight-datasheet/) | [VOXL Flight](/voxl-flight-datasheet/) |
| Flight Control | PX4 on [VOXL Flight](/voxl-flight-datasheet/) | PX4 on [VOXL Flight](/voxl-flight-datasheet/) |
| Image Sensors | [Stereo](/voxl-stereo-camera-datasheet/), [Tracking](/voxl-tracking-camera-datasheet/), [4k High-resolution](/voxl-hires-camera-datasheet/) |  [Stereo](/voxl-stereo-camera-datasheet/), [Tracking](/voxl-tracking-camera-datasheet/), [4k High-resolution](/voxl-hires-camera-datasheet/) |
| Communication | WiFi built-in, [LTE](/lte-modem-and-usb-add-on-datasheet/) or [Microhard](/microhard-add-on-datasheet/) optional |  WiFi built-in, [LTE](/lte-modem-and-usb-add-on-datasheet/) or [Microhard](/microhard-add-on-datasheet/) optional |
| Remote Control | Configured for spektrum, compatible with most DIY R/C systems through expansion I/O on [Flight Core](/flight-core-datasheet/) |  Configured for spektrum, compatible with most DIY R/C systems through expansion I/O on [Flight Core](/flight-core-datasheet/) |
| Power Module | [Power Module v2](power-module-v2-datasheet) |  [Power Module v2](power-module-v2-datasheet) |
| Battery | Up to 5S (not included, [recommended - select XT60 connector](https://www.thunderpowerrc.com/products/tp3400-4spx25?variant=31080855339072)) | Up to 5S (not included, [recommended - select XT60 connector](https://www.thunderpowerrc.com/products/tp3400-4spx25?variant=31080855339072)) |
| Additional Components | N/A | Gimbal w/o Camera, details below |

* NOTE: very early m500 shipments have ESCs with "black/red/white" wires, where latter shipments have "black/red/yellow" wires.  These ESCs have slightly different performance and shouldn't be mixed together on the same vehicle.

## System Overview

[View in fullsize](/images/datasheet/m500/VOXL-m500-R2-architecture.png){:target="_blank"}
![m500-.png](/images/datasheet/m500/VOXL-m500-R2-architecture.png)

## PX4 Tuning Parameters

Our engineers (namely James!) have spent a lot of time finely tuning this vehicle, and the [parameters are available here](https://gitlab.com/voxl-public/px4-parameters)

## Connectivity for Remote Operation

| Connectivity Option | Use Case | Details |
| --- | --- | --- |
| Spektrum R/C | Manual remote control of the vehicle | [Configure](https://docs.modalai.com/configure-rc-radio/) |
| WiFi | Short range (~100m) IP connectivity for debug and nearby flights | [Setup](https://docs.modalai.com/wifi-setup/) |
| LTE | Long Range, BVLOS operation | [User Manual](https://docs.modalai.com/lte-modem-manual/) <br> [Datasheet](https://docs.modalai.com/lte-modem-and-usb-add-on-datasheet/) |
| Microhard | Medium Range (1-3km) IP connectivity for control, telemetry and video | |

## Gimbal Specs (Optional Add-on)

### VOXL-m500-R2-G1

<span style="color:red">Camera not included</span>

For this gimbal, the recommended camera is something like the `Mobius Pro Mini Action Camera` based on form factor alone.  We are not qualifying this camera's performance, but based on size, weight and geometry, this camera has been shown to work well in our testing wih the gimbal referenced below.

|                     | Details |
| ---                 | ---     |
| Gimbal              | [HMG MA3D](https://hobbyking.com/en_us/hmg-ma3d-3-axis-brushless-gimbal-for-mobius-camera.html) |
| Regulator           | [D24V22F12](https://www.pololu.com/product/2855) |
| Control             | Pitch and yaw through AUX1 and AUX2 |
|                     | [See User Guide](/voxl-m500-gimbal-user-guide/) |
