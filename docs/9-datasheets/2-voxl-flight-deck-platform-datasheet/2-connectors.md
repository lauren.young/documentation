---
layout: default
title: VOXL Flight Deck Connectors
parent: VOXL Flight Deck Platform Datasheet
grand_parent: Datasheets
nav_order: 2
permalink: /voxl-flight-deck-datasheet-connectors/
---

# VOXL Flight Deck Connectors
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Primary Flight Deck Connections

Below describes the primary connections needed to get the Flight Deck configured on a vehicle and flying.

![voxl-flight-deck-connectors](/images/datasheet/voxl-flight-deck/voxl-flight-deck-connectors.png)

|    | Name | Description |
|--- |--- |--- |
| A  | GPS Input | Dronecode Compliant GPS connection, 9optional when using [ModalAI's PX4 branch)(/flight-core-firmware/), GPS not included |
| B  | USB to QGroundControl | Connect VOXL Flight to QGroundControl for configuring PX4 using included cable |
| C  | RC Input | Connect to RC Reciever using included cable, RC Reciever not included |
| D  | USB to VOXL (under) | Connect to companion computer using adb over USB using Micro USB cable, cable not included |
| E  | PX4 MicroSD Card Slot (under) | For PX4 logging, [information on supported cards](https://dev.px4.io/v1.9.0/en/log/logging.html#sd-cards), SD card not included |
| F  | Power Input | Connects included [VOXL Power Module v2](/power-module-v2-datasheet/) using included cable |
| G  | PWM Output | Connects included PWM Breakout Board using included cable |

## VOXL Flight Connections

The VOXL Flight Deck consists of a VOXL Flight, with [the detailed pinouts described here](/voxl-flight-datasheet-connectors/)
