---
layout: default
title: VOXL Quickstart
nav_order: 4
parent: Quickstarts
has_children: true
permalink: /voxl-quickstarts/
has_toc: false
---

# VOXL Quickstart
{: .no_toc }

This section contains simple topics geared towards helping you get up and running with your VOXL Core for the first time. They are in a logical order that guides you through tasks a normal developer would typically go through to get started.

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

![voxl-dk](/images/quickstart/voxl/voxl-dk.jpg)

## Guided Setup

- [Start Here](/voxl-quickstart-requirements/)

## Helpful Links

- [VOXL Support Forum](https://forum.modalai.com/category/9/voxl)
- [User Guide - VOXL Features](/voxl-quickstart-requirements/)
- [User Guide - Flight Core Features](/flight-core-getting-started/)
- [Data Sheet](/voxl-flight-datasheet/)

## Connectors - Top

![VOXL Core Top](../../images/datasheet/voxl-core-top.png)

| Connector | Summary |
| --- | --- | --- |
| J2  | Hires 4k Image Sensor (CSI0) |
| J3  | Stereo Image Sensor (CSI1) |
| J6  | Cooling Fan Connector |
| J7  | BLSP6 (GPIO) and BLSP9 (UART): External GPS/MAG |
| J13 | Expansion B2B |
| J14 | Integrated GNSS Antenna Connection |

## Connectors - Bottom

![VOXL Core Top](../../images/datasheet/voxl-core-bottom.png)

| Connector | Summary | |
| --- | --- | --- |
| J1 | 5V DC IN, I2C to power cable “APM” |
| J4 |  Tracking/Optic Flow Image Sensor (CSI2) |
| J8 | USB 3.0 OTG |
| J9 | Micro-SD Slot |
| J10 | BLSP7 UART and I2C off-board 
| J11 | BLSP12 UART and I2C off-board (SPEKTRUM) |
| J12 | BLSP5 UART and GPIO off-board (ESC) |
| Wi-Fi | External Wi-Fi antenna connections |
