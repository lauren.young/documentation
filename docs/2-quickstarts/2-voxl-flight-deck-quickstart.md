---
layout: default
title: VOXL Flight Deck Quickstart
nav_order: 2
parent: Quickstarts
has_children: true
permalink: /voxl-flight-deck-quickstart/
has_toc: false
---

# VOXL Flight Deck Quickstart
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

![voxlf-dk](/images/quickstart/flight-deck/flight-deck.jpg)

## Guided Setup

- [Start Here](/voxl-flight-deck-getting-started/)

## Helpful Links

- [VOXL Flight Deck Support Forum](https://forum.modalai.com/category/7/voxl-flight-deck)
- [User Guide](/voxl-flight-deck-user-guide/)
- [Data Sheet](/voxl-flight-deck-functional-description/)
- [PWM ESC Calibration](/flight-core-pwm-esc-calibration/)

## Connections

![voxl-flight-deck-connectors](/images/datasheet/voxl-flight-deck/voxl-flight-deck-connectors.png)
