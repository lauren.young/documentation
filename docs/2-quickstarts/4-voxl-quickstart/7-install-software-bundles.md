---
layout: default
title: Install & Upgrade Software
parent: VOXL Quickstart
grand_parent: Quickstarts
nav_order: 7
permalink: /install-software-bundles/
---

# Install & Upgrade Software
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Prerequisites

These instructions assume the user has [set up WiFi](/wifi-setup/).


## Overview

VOXL's software stack is broken up into 3 parts, each with their own version and ability to upgrade independently or all together.

- VOXL System Image: This includes the root file system and bootloader partitions. It is only necessary to update the system image to add new hardware driver support.
- VOXL Factory Bundle: This contains proprietary and 3rd party software packages (updated infrequently and now included with the system image starting with v2.3.0)
- VOXL Software Suite (`voxl-suite`): This contains precompiled packages of ModalAI-developed open-source software (updated regularly), which can be installed over the internet using the `opkg` pakage manager (similar to the apt package manager).
  - Note: previous to System Image 2.3.0, a `voxl-software-bundle` was used which was similar to the factory bundle.

The system image with built-in factory bundle is available at [https://developer.modalai.com/asset](https://developer.modalai.com/asset) and can be installed with the [flash system image instructions](/flash-system-image/).


## Configure OPKG package manager repository

Starting with System Image 2.3.0, the opkg package manager is already configured to look for and install packages from [http://voxl-packages.modalai.com/stable](http://voxl-packages.modalai.com/stable)

This package repository is updated frequently with new packages, new features, and bugfixes. It is split into stable and dev repositories. We suggest using the stable repository unless you are working with ModalAI on new/experimental features that you need access to.

If you are on a system image older than v2.3.0 and want to start using the new package repository, simple edit the `/etc/opkg/opkg.conf` file to include the following repository source information at the end of the file:

```bash
yocto:/# vi /etc/opkg/opkg.conf
```

```
.
.
.
src/gz stable http://voxl-packages.modalai.com/stable
## uncomment line below to enable development repository
#src/gz dev http://voxl-packages.modalai.com/dev
```

This is also where you will select between the stable and dev repositories.


## Check to see what's already installed

You check to see if voxl-suite is installed along with any other ModalAI packages with `opkg list-installed`. A recent install of voxl-suite should look something like this:

```bash
yocto:/# opkg list-installed | grep "voxl"
libvoxl_io - 0.4.1
voxl-cam-manager - 0.2.2
voxl-docker-support - 1.1.1
voxl-hal3-tof-cam-ros - 0.0.2
voxl-modem - 0.9.3
voxl-nodes - 0.0.8
voxl-rtsp - 1.0.2
voxl-suite - 0.1.2
voxl-utils - 0.5.1
voxl-vision-px4 - 0.6.2
voxl_imu - 0.0.4
yocto:/#
```


## Installing/Upgrading voxl-suite

Ensure VOXL is connected to the internet (see how to [setup WiFi](/wifi-setup/) for more details). Then run `opkg update` to pull the latest package info. This is similar to "apt update" which you may already be familiar with.

```bash
yocto:/# opkg update
```

If `voxl-suite` is not yet installed because you are on an older system image or elected not to install it during the system image flashing process, you can install it with the `opkg install` command. This will install the suite of ModalAI developed open-source supporting software packages.

```bash
yocto:/# opkg install voxl-suite
```

In the future, you can upgrade all the packages in voxl-suite with `opkg update` and `opkg upgrade`. Note this will require VOXL to have an internet connection.

```bash
yocto:/# opkg update
yocto:/# opkg upgrade
```




## Next Steps

Now we can [configure cameras](/configure-cameras/).

