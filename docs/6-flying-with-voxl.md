---
layout: default
title: Flying with VOXL
nav_order: 6
has_children: true
permalink: /flying-with-voxl/
---

# Flying with VOXL
{: .no_toc }

This section contains instructions on how to use VOXL as a companion computer alongside a PX4 flight controller to enable flying with Visual Inertial Odometry (VIO), Visual Obstacle Avoidance (VOA), wireless telemetry over LTE.

VOXL can be used as an ultra-lightweight companion computer to either the [Modal AI Flight Core](/flight-core-manual/) or a Pixhawk flight controller. This section of the manual focuses on the use of VOXL in conjunction with a PX4 Flight Controller. For details on using the ModalAI Flight Core by itself, follow the [ModalAI Flight Core manual](/flight-core-manual/)


<iframe width="560" height="315" src="https://www.youtube.com/embed/qwSKEOF2rFM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<iframe width="560" height="315" src="https://www.youtube.com/embed/wrqZFTiHyTU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---